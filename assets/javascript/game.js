// Hangman

// Base Variables
// Start with a list/array of words

var wordList = ["superman", "batman", "wolverine", "spiderman", "hulk", "ironman", "deadpool", "wonderwoman", "daredevil"];
var word;
var counter;
var userGuess;
var correctLetters = [];
var wrongLetters = [];
var wins = 0;
var losses = 0;
var winTracker = document.getElementById("wins");
var guessTracker = document.getElementById("wrongGuesses");


// Start game function

function start () {
// Randomly select a word

  word = wordList[Math.floor(Math.random() * wordList.length)];
  console.log(word); // success!

// Set the counter for 7 and display it on page
 
  counter = 7;
  document.getElementById("counter").innerHTML = counter;
  guessTracker.innerHTML = "";

// Determine how many letters are in word

  console.log(word.length); // sucess!

// Create a space("_") for each letter

  for (var i = 0; i < word.length; i++) {
    correctLetters[i] = "_";
  };

// Display each space on page (current word)

  document.getElementById("answer").innerHTML = correctLetters.join(" ");

}; // End of start()


// Check letter function

function checkLetter() {

// User can type any key

  document.onkeyup = function(event) {

// Detect user's typed keys

// Is the key a letter (keycode between 90 and 65)
    if (!(event.which <= 90 && event.which >= 65)) return
    
    userGuess = event.key.toLowerCase();
    console.log(userGuess);

// Determines when/if letter is found
  var found = false;

// Compare user's keys to letters in word

    for (var i = 0; i < word.length; i++) {
      if (word[i] === userGuess) {
        console.log("match")

// If key matches letter in word, replace the space("_") on page
      
        correctLetters[i] = userGuess
        document.getElementById("answer").innerHTML = correctLetters.join(" ")
    
        found = true;
      } 
    };

// If key doesn't match, add to letters guessed
  
    if (found && correctLetters.join("") !== word) return;

// If counter is still above 0 and correctLetters match word,

    if (counter > 0 && correctLetters.join("") == word) {
  	  setTimeout(function () {

// +1 to wins, update HTML, and clear the score board
  		
  		  wins += 1;
  		  winTracker.innterHTML = wins;
          correctLetters = [];
          
// Display image in card upon winning
    
    if (word === "superman") {
      document.getElementById("logoTitle").innerHTML = "Superman";
      document.getElementById("logos").src="assets/images/superman.png";
    }
    if (word === "batman") {
      document.getElementById("logoTitle").innerHTML = "Batman";
      document.getElementById("logos").src="assets/images/batman.png";
    }
    if (word === "wolverine") {
      document.getElementById("logoTitle").innerHTML = "Wolverine";
      document.getElementById("logos").src="assets/images/wolverine.png";
    }
    if (word === "spiderman") {
      document.getElementById("logoTitle").innerHTML = "Spider-Man";
      document.getElementById("logos").src="assets/images/spiderman.png";
    }
    if (word === "hulk") {
      document.getElementById("logoTitle").innerHTML = "Hulk";
      document.getElementById("logos").src="assets/images/hulk.png";
    }
    if (word === "ironman") {
      document.getElementById("logoTitle").innerHTML = "Iron Man";
      document.getElementById("logos").src="assets/images/ironman.png";
    }
    if (word === "deadpool") {
      document.getElementById("logoTitle").innerHTML = "Deadpool";
      document.getElementById("logos").src="assets/images/deadpool.png";
    }
    if (word === "wonderwoman") {
      document.getElementById("logoTitle").innerHTML = "Wonder Woman";
      document.getElementById("logos").src="assets/images/wonderwoman.png";
    }
    if (word === "daredevil") {
      document.getElementById("logoTitle").innerHTML = "Daredevil";
      document.getElementById("logos").src="assets/images/daredevil.png";
    };        

// Included a timeout function and return to restart the game
    
      setTimeout(function() {
      start(); }, 50);
  	  }, 50);
  	  return;
    };

// If the userGuess has not already been guessed, add to wrongLetters.
  
    if (wrongLetters.indexOf(userGuess) < 0) {
  	  wrongLetters.push(userGuess);
  	  document.getElementById("wrongGuesses").innerHTML = wrongLetters.join(" ")

// -1 to counter and write to page
  	
  	  counter--;
  	  console.log(counter);
  	  document.getElementById("counter").innerHTML = counter;

// "Game Over" when counter reaches 0
    
      if (counter === 0) {
    	
// +1 to loses
    	  
    	  document.getElementById("losses").innerHTML = losses + 1;
    	
// Confirm prompt to restart teh game
    	  
    	  confirm("YOU LOST... play again?"); {
            losses++;
            counter = 7;
            correctLetters = [];
            wrongLetters = [];
            start();
    	  }; 

      }; // End of "Game Over"

    }; //  End of wrongLetters +

  }; // End of document.onkeyup()

}; // End of checkLetter()

start();
checkLetter();